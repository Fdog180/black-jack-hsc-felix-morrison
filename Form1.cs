﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Blackjack
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            Shuffle();
        }


        List<Card> playercardList = new List<Card>()
        {
            new Card() { Value  = 0, Name = "null", Image = "null" }
        };

        List<Card> bankercardList = new List<Card>()
        {
            new Card() { Value  = 0, Name = "null", Image = "null" }
        };

        List<Card> player1cardList = new List<Card>()
        {
            new Card(){ Value = 0, Name = "null", Image = "Null"}
        };

        List<Card> player2cardList = new List<Card>()
        {
            new Card(){ Value = 0, Name = "null", Image = "Null"}
        };

        List<Card> player3cardList = new List<Card>()
        {
            new Card(){ Value = 0, Name = "null", Image = "Null"}
        };


        int playercardSum = 0;
        int bankercardSum = 0;
        int player1cardSum = 0;
        int player2cardSum = 0;
        int player3cardSum = 0;
        int playermoney = 100;
        int playerdifficulty = 17;
        int dealerdifficulty = 17;
        int pot = 0;
        Random random = new Random();
        List<int> usedCards = new List<int>();
        List<PictureBox> bankerbox = new List<PictureBox>();
        List<PictureBox> playerbox = new List<PictureBox>();
        List<PictureBox> player1box = new List<PictureBox>();
        List<PictureBox> player2box = new List<PictureBox>();
        List<PictureBox> player3box = new List<PictureBox>();
        private int selectRandomCard()
        {
            int randomCard;
            randomCard = random.Next(0, deck.Count);
            return randomCard;
        }

        // Game Values
        private void playerLost()
        {
            resultLabel.Text = ("YOU LOSE");
            pot = 0;
            hitMeButton.Visible = false;
            button1.Visible = false;
            dealButton.Visible = false;
        }
        private void playerWin()
        {
            resultLabel.Text = ("YOU WIN");
            playermoney += pot * 2;
            pot = 0;
            hitMeButton.Visible = false;
            button1.Visible = false;
            dealButton.Visible = false;
        }
        private void playerTie()
        {
            resultLabel.Text = ("YOU TIED WITH THE DEALER");
            playermoney += pot;
            pot = 0;
            hitMeButton.Visible = false;
            button1.Visible = false;
            dealButton.Visible = false;
        }

        private void player1Lost()
        {
            label10.Text = ("LOST");

        }
        private void player1Won()
        {
            label10.Text = ("WON");
        }
        private void player1Tie()
        {
            label10.Text = ("TIED");
        }

        private void player2Lost()
        {
            label11.Text = ("LOST");

        }
        private void player2Won()
        {
            label11.Text = ("WON");
        }
        private void player2Tie()
        {
            label11.Text = ("TIED");
        }

        private void player3Lost()
        {
            label12.Text = ("LOST");

        }
        private void player3Won()
        {
            label12.Text = ("WON");
        }
        private void player3Tie()
        {
            label12.Text = ("TIED");
        }

        private void noMoney()
        {
            resultLabel.Location = new Point(333, 260);
            resultLabel.Text = ("Wow, didn't you know the howse always wins?");
        }

        #region creationof52carddeck

        List<Card> deck = new List<Card>()
            {
                #region spades

                new Card() { Value  = 2, Name = "Two Spades", Image = "2S.png" },
                new Card() { Value = 3, Name = "Three Spades", Image = "3S.png"},
                new Card() { Value = 4, Name =  "Four Spades", Image = "4S.png"},
                new Card() { Value = 5, Name = "Five Spades", Image = "5S.png" },
                new Card() { Value = 6, Name = "Six Spades", Image = "6S.png" },
                new Card() { Value = 7, Name = "Seven Spades", Image = "7S.png" },
                new Card() { Value = 8, Name = "Eight Spades", Image = "8S.png" },
                new Card() { Value = 9, Name = "Nine Spades", Image = "9S.png" },
                new Card() { Value = 10, Name = "Ten Spades", Image = "10S.png" },
                new Card() { Value = 10, Name = "Jack Spades", Image = "JS.png" },
                new Card() { Value = 10, Name = "Queen Spades", Image = "QS.png" },
                new Card(){ Value = 10, Name = "King Spades", Image = "KS.png" },
                new Card(){ Value = 11, Name = "Ace Spades", Image = "AS.png" },

                #endregion

                #region diamonds

                new Card() { Value  = 2, Name = "Two Diamonds", Image = "2D.png" },
                new Card() { Value = 3, Name = "Three Diamonds", Image = "3D.png" },
                new Card() { Value = 4, Name =  "Four Diamonds", Image = "4D.png"},
                new Card() { Value = 5, Name = "Five Diamonds", Image = "5D.png" },
                new Card() { Value = 6, Name = "Six Diamonds", Image = "6D.png" },
                new Card(){ Value = 7, Name = "Seven Diamonds", Image = "7D.png" },
                new Card() { Value = 8, Name = "Eight Diamonds", Image = "8D.png" },
                new Card() { Value = 9, Name = "Nine Diamonds", Image = "9D.png" },
                new Card() { Value = 10, Name = "Ten Diamonds", Image = "10D.png" },
                new Card() { Value = 10, Name = "Jack Diamonds", Image = "JD.png" },
                new Card() { Value = 10, Name = "Queen Diamonds", Image = "QD.png" },
                new Card(){ Value = 10, Name = "King Diamonds", Image = "KD.png" },
                new Card(){ Value = 11, Name = "Ace Diamonds", Image = "AD.png" },

                #endregion

                #region clubs

                new Card() { Value  = 2, Name = "Two Clubs", Image = "2C.png" },
                new Card() { Value = 3, Name = "Three Clubs", Image = "3C.png" },
                new Card() { Value = 4, Name =  "Four Clubs", Image = "4C.png"},
                new Card() { Value = 5, Name = "Five Clubs", Image = "5C.png" },
                new Card() { Value = 6, Name = "Six Clubs", Image = "6C.png" },
                new Card(){ Value = 7, Name = "Seven Clubs", Image = "7C.png" },
                new Card() { Value = 8, Name = "Eight Clubs", Image = "8C.png" },
                new Card() { Value = 9, Name = "Nine Clubs", Image= "9C.png" },
                new Card() { Value = 10, Name = "Ten Clubs", Image = "10C.png" },
                new Card() { Value = 10, Name = "Jack Clubs", Image = "JC.png" },
                new Card() { Value = 10, Name = "Queen Clubs", Image = "QC.png" },
                new Card(){ Value = 10, Name = "King Clubs", Image = "KC.png" },
                new Card(){ Value = 11, Name = "Ace Clubs", Image = "AC.png" },

                #endregion

                #region hearts

                new Card() { Value  = 2, Name = "Two Hearts", Image = "2H.png" },
                new Card() { Value = 3, Name = "Three Hearts", Image = "3H.png" },
                new Card() { Value = 4, Name =  "Four Hearts", Image = "4H.png"},
                new Card() { Value = 5, Name = "Five Hearts", Image = "5H.png" },
                new Card() { Value = 6, Name = "Six Hearts", Image = "6H.png" },
                new Card(){ Value = 7, Name = "Seven Hearts", Image = "7H.png" },
                new Card() { Value = 8, Name = "Eight Hearts", Image = "8H.png" },
                new Card() { Value = 9, Name = "Nine Hearts", Image = "9H.png" },
                new Card() { Value = 10, Name = "Ten Hearts", Image = "10H.png" },
                new Card() { Value = 10, Name = "Jack Hearts", Image = "JH.png" },
                new Card() { Value = 10, Name = "Queen Hearts", Image = "QH.png" },
                new Card(){ Value = 10, Name = "King Hearts", Image = "KH.png" },
                new Card(){ Value = 11, Name = "Ace Hearts", Image = "AH.png" }

                #endregion
            };

        #endregion

        private void displayCardBack(PictureBox picturebox)
        {
            picturebox.ImageLocation = "b1fv.png";
            picturebox.SizeMode = PictureBoxSizeMode.AutoSize;
        }

        private void Form1_Load(object sender, EventArgs e)
        {

            resetGame();
        }

        private void Shuffle()
        {
            Random r = new Random();
            int shuffleamount = 100000;

            for (int i = 0; i <= shuffleamount; i++)
            {
                Card temp = new Card();
                int card1 = r.Next(deck.Count() - 1);
                int card2 = r.Next(deck.Count() - 1);

                temp = deck[card1];
                deck[card1] = deck[card2];
                deck[card2] = temp;
            }
        }

        private void resetGame()
        {
            resultLabel.Text = null;
            label8.Text = playerdifficulty.ToString();
            label9.Text = playerdifficulty.ToString();

            displayCardBack(pictureBox1);
            displayCardBack(pictureBox2);
            displayCardBack(pictureBox3);
            displayCardBack(pictureBox4);
            displayCardBack(cardBoxP11);
            displayCardBack(cardBoxP21);
            displayCardBack(cardBoxP31);


            //displayCardBack(pictureBox6);
            foreach (PictureBox pb in playerbox)
            {
                this.Controls.Remove(pb);
            }
            playerbox = new List<PictureBox>();

            foreach (PictureBox pb in bankerbox)
            {
                this.Controls.Remove(pb);
            }
            bankerbox = new List<PictureBox>();

            foreach (PictureBox pb in player1box)
            {
                this.Controls.Remove(pb);
            }
            player1box = new List<PictureBox>();

            foreach (PictureBox pb in player2box)
            {
                this.Controls.Remove(pb);
            }
            player2box = new List<PictureBox>();

            foreach (PictureBox pb in player3box)
            {
                this.Controls.Remove(pb);
            }
            player3box = new List<PictureBox>();

            playercardSum = 0;
            bankercardSum = 0;
            playercardList.Clear();
            bankercardList.Clear();
            usedCards.Clear();
            resultLabel.Text = "Your Turn";
            label3.Text = playermoney.ToString();
            label4.Text = pot.ToString();
            hitMeButton.Visible = true;
            button1.Visible = true;
            dealButton.Visible = true;
        }
        //start the game 
        private void startButton_Click(object sender, EventArgs e)
        {
            Shuffle();

            if (pot != 0)
            {
                if (playercardSum > 0)
                {
                    resultLabel.Text = String.Format
                    ("Already started.");
                }

                else
                {

                    playercardSum = 0;
                    bankercardSum = 0;
                    player1cardSum = 0;
                    player2cardSum = 0;
                    player3cardSum = 0;

                    #region init player
                    int randomCard1 = selectRandomCard();
                    Card card1 = deck[randomCard1];
                    usedCards.Add(randomCard1);
                    int randomCard2 = selectRandomCard();


                    while (usedCards.Contains(randomCard2))
                    {
                        randomCard2 = selectRandomCard();
                    }
                    randomCard2 = 1 * randomCard2;

                    Card card2 = deck[randomCard2];
                    usedCards.Add(randomCard2);

                    playercardList.Add(card1);
                    playercardList.Add(card2);

                    pictureBox1.ImageLocation = card1.Image;
                    pictureBox1.SizeMode = PictureBoxSizeMode.AutoSize;

                    pictureBox2.ImageLocation = card2.Image;
                    pictureBox2.SizeMode = PictureBoxSizeMode.AutoSize;
                    #endregion

                    #region init banker
                    int randomCard3 = selectRandomCard();
                    while (usedCards.Contains(randomCard3))
                    {
                        randomCard3 = selectRandomCard();
                    }
                    randomCard3 = 1 * randomCard3;
                    Card card3 = deck[randomCard3];
                    usedCards.Add(randomCard3);

                    bankercardList.Add(card3);

                    pictureBox4.ImageLocation = card3.Image;
                    pictureBox4.SizeMode = PictureBoxSizeMode.AutoSize;

                    #endregion

                    #region initPlayer1
                    int randomCard4 = selectRandomCard();
                    while (usedCards.Contains(randomCard4))
                    {
                        randomCard4 = selectRandomCard();
                    }
                    randomCard4 = 1 * randomCard4;
                    Card card4 = deck[randomCard4];
                    usedCards.Add(randomCard4);

                    player1cardList.Add(card4);


                    cardBoxP11.ImageLocation = card4.Image;
                    cardBoxP11.SizeMode = PictureBoxSizeMode.AutoSize;

                    #endregion

                    #region initPlayer2
                    int randomCard5 = selectRandomCard();
                    while (usedCards.Contains(randomCard5))
                    {
                        randomCard5 = selectRandomCard();
                    }
                    randomCard5 = 1 * randomCard5;
                    Card card5 = deck[randomCard5];
                    usedCards.Add(randomCard5);

                    player2cardList.Add(card5);

                    cardBoxP21.ImageLocation = card5.Image;
                    cardBoxP21.SizeMode = PictureBoxSizeMode.AutoSize;

                    #endregion

                    #region intitPlayer3
                    int randomCard6 = selectRandomCard();
                    while (usedCards.Contains(randomCard6))
                    {
                        randomCard6 = selectRandomCard();
                    }
                    randomCard6 = 1 * randomCard6;
                    Card card6 = deck[randomCard6];
                    usedCards.Add(randomCard6);

                    player3cardList.Add(card6);

                    cardBoxP31.ImageLocation = card6.Image;
                    cardBoxP31.SizeMode = PictureBoxSizeMode.AutoSize;
                    #endregion

                    sumPlayerCards();




                }

            }
            else if (pot == 0)
            {
                resultLabel.Text = ("You need to bet");
            }


        }

        private void sumPlayerCards()
        {
            playercardSum = 0;
            for (int i = 0; i < playercardList.Count; i++)
            {
                playercardSum += playercardList[i].Value;
            }

            {
                foreach (Card c in playercardList)
                {
                    if (c.Value == 11)
                    {
                        playercardSum -= 10;
                        if (playercardSum <= 21)
                        {
                            break;
                        }
                    }
                }
            }
        }
        private void sumBankerCards()
        {
            bankercardSum = 0;
            for (int i = 0; i < bankercardList.Count; i++)
            {
                bankercardSum += bankercardList[i].Value;
            }
            if (bankercardSum > 21)
            {
                foreach (Card c in bankercardList)
                {
                    if (c.Value == 11)
                    {
                        bankercardSum -= 10;
                        if (bankercardSum <= 21)
                        {
                            break;
                        }
                    }
                }
            }
        }
        private void sumPlayer1Cards()
        {
            player1cardSum = 0;
            for (int i = 0; i < player1cardList.Count; i++)
            {
                player1cardSum = player1cardList[i].Value;
            }
            if (player1cardSum > 21)
            {
                foreach (Card c in player1cardList)
                {
                    if (c.Value == 11)
                    {
                        player1cardSum -= 10;
                        if (player1cardSum <= 21)
                        {
                            break;

                        }
                    }
                }
            }
        }
        private void sumPlayer2Cards()
        {
            player2cardSum = 0;
            for (int i = 0; i < player2cardList.Count; i++)
            {
                player2cardSum = player2cardList[i].Value;
            }
            if (player2cardSum > 21)
            {
                foreach (Card c in player2cardList)
                {
                    if (c.Value == 11)
                    {
                        player2cardSum -= 10;
                        if (player2cardSum <= 21)
                        {
                            break;

                        }
                    }
                }
            }
        }
        private void sumPlayer3Cards()
        {
            player3cardSum = 0;
            for (int i = 0; i < player3cardList.Count; i++)
            {
                player3cardSum = player3cardList[i].Value;
            }
            if (player3cardSum > 21)
            {
                foreach (Card c in player3cardList)
                {
                    if (c.Value == 11)
                    {
                        player3cardSum -= 10;
                        if (player3cardSum <= 21)
                        {
                            break;

                        }
                    }
                }
            }
        }

        // Hit Button
        private void DealButton_Click(object sender, EventArgs e)
        {
            sumPlayerCards();
            Shuffle();

            if (pot == 0)
            {
                resultLabel.Text = ("You need to bet");
            }
            else if (pot != 0)
            {
                if (playercardSum == 0)
                {
                    resultLabel.Text = "Click the Start button...";
                    //displayCardBack(pictureBox3);
                }

                else
                {
                    playercardSum = 0;
                    int randomCard = selectRandomCard();
                    Card card = deck[randomCard];
                    usedCards.Add(randomCard);

                    if (usedCards.Contains(randomCard)) randomCard = selectRandomCard();
                    else randomCard = 1 * randomCard;

                    sumPlayerCards();

                    if (playercardSum > 21 && randomCard >= 11)
                    {
                        randomCard = 1;
                        sumPlayerCards();
                    }

                    //player new card
                    PictureBox p3 = new PictureBox();
                    p3.Width = 71;
                    p3.Height = 96;
                    p3.Location = new Point(532 + playerbox.Count * 75, 290);
                    p3.ImageLocation = card.Image;
                    p3.SizeMode = PictureBoxSizeMode.AutoSize;
                    this.Controls.Add(p3);
                    playerbox.Add(p3);
                    playercardList.Add(card);
                }
                sumPlayerCards();
                if (playercardSum > 21)
                {
                    playerLost();
                }
            }
        }

           // Stand button
        private void PlayerStopButton_Click(object sender, EventArgs e)
        {
            Shuffle();
            if (playercardSum == 0)
            {
                resultLabel.Text = "Click the Start button...";
                return;
            }

            else
            {
                // bankermove();
                player3move();
                player2move();
                player1move();
                bankermove();
            }

            if (bankercardSum == playercardSum)
            {
                playerTie();
            }
            else if (playercardSum < bankercardSum && bankercardSum <= 21)
            {
                playerLost();
            }
            else if (playercardSum <= 21 && bankercardSum < playercardSum)
            {
                playerWin();
            }
            else if (bankercardSum > 21)
            {
                playerWin();
            }

            CheckPlayer1();
            CheckPlayer2();
            CheckPlayer3();
        }
        private void resetButton_Click(object sender, EventArgs e)
        {
            resultLabel.Location = new Point(425, 260);
            resetGame();
            Shuffle();
        }

        // Betting SYSTEM
        private void Button3_Click(object sender, EventArgs e)
        {
            if (playermoney == 0)
            {
                noMoney();
            }
            else if (playermoney >= 10)
            {
                pot += 10;
                playermoney -= 10;
                label3.Text = playermoney.ToString();
                label4.Text = pot.ToString();
            }
            else if (playermoney < 20)
            {
                resultLabel.Text = ("Not enough money!");
            }

        }
        private void Button6_Click(object sender, EventArgs e)
        {
            if (playermoney == 0)
            {
                noMoney();

            }
            else if (playermoney >= 20)
            {
                pot += 20;
                playermoney -= 20;
                label3.Text = playermoney.ToString();
                label4.Text = pot.ToString();
            }
            else if (playermoney < 20)
            {
                resultLabel.Text = ("Not enough money!");

            }
        }
        private void Button4_Click(object sender, EventArgs e)
        {
            if (playermoney == 0)
            {
                noMoney();
            }

            else if (playermoney < 50)
            {
                resultLabel.Text = String.Format
                ("Not enough money!");
            }
            else if (playermoney >= 50)
            {
                pot += 50;
                playermoney -= 50;
                label3.Text = playermoney.ToString();
                label4.Text = pot.ToString();
            }
        }
        private void Button5_Click(object sender, EventArgs e)
        {
            if (playermoney == 0)
            {
                noMoney();
            }
            else if (playermoney < 100)
            {
                resultLabel.Text = String.Format
                ("Not enough money!");
            }
            else if (playermoney >= 100)
            {
                playermoney -= 100;
                pot += 100;
                label3.Text = playermoney.ToString();
                label4.Text = pot.ToString();
            }

        }
        private void Button9_Click(object sender, EventArgs e)
        {
            if (playermoney == 0)
            {
                noMoney();
            }
            else if (playermoney > 0)
            {
                pot = playermoney;
                playermoney = 0;
                label3.Text = playermoney.ToString();
                label4.Text = pot.ToString();
            }



        }
        private void Button10_Click(object sender, EventArgs e)
        {
            if (playermoney == pot / 2)
            {
                playermoney -= pot;
                pot = pot * 2;
                label3.Text = playermoney.ToString();
                label4.Text = pot.ToString();
            }
            else
                resultLabel.Text = ("Not enough money to double");

        }

        // DEALER AND PLAYER DIFFICULTY
        private void Button11_Click(object sender, EventArgs e)
        {
            if (playerdifficulty < 20)
            {
                playerdifficulty += 1;
                label8.Text = playerdifficulty.ToString();
            }
        }

        private void Button12_Click(object sender, EventArgs e)
        {
            if (playerdifficulty > 10)
            {
                playerdifficulty -= 1;
                label8.Text = playerdifficulty.ToString();
            }
        }

        private void Button13_Click(object sender, EventArgs e)
        {
            if (dealerdifficulty < 20)
            {
                dealerdifficulty += 1;
                label9.Text = dealerdifficulty.ToString();
            }
        }

        private void Button14_Click(object sender, EventArgs e)
        {
            if (dealerdifficulty > 10)
            {
                dealerdifficulty -= 1;
                label9.Text = dealerdifficulty.ToString();
            }
        }


        // Checking if computer players have won/lost

        private void CheckPlayer1()
        {
            if (bankercardSum == player1cardSum)
            {
                player1Tie();
            }
            else if (player1cardSum < bankercardSum && bankercardSum <= 21)
            {
                player1Lost();
            }
            else if (player1cardSum <= 21 && bankercardSum < player1cardSum)
            {
                player1Won();
            }
            else if (bankercardSum > 21)
            {
                player1Won();
            }
        }

        private void CheckPlayer2()
        {
            if (bankercardSum == player2cardSum)
            {
                player2Tie();
            }
            else if (player2cardSum < bankercardSum && bankercardSum <= 21)
            {
                player2Lost();
            }
            else if (player2cardSum <= 21 && bankercardSum < player2cardSum)
            {
                player2Won();
            }
            else if (bankercardSum > 21)
            {
                player2Won();
            }
        }

        private void CheckPlayer3()
        {
            if (bankercardSum == player3cardSum)
            {
                player3Tie();
            }
            else if (player3cardSum < bankercardSum && bankercardSum <= 21)
            {
                player3Lost();
            }
            else if (player3cardSum <= 21 && bankercardSum < player3cardSum)
            {
                player3Won();
            }
            else if (bankercardSum > 21)
            {
                player3Won();
            }
        }

        //AI MOVES
        private void bankermove()
        {
            int randomCard = selectRandomCard();
            while (usedCards.Contains(randomCard))
            {
                randomCard = selectRandomCard();
            }
            randomCard = 1 * randomCard;
            Card card = deck[randomCard];
            usedCards.Add(randomCard);

            bankercardList.Add(card);

            pictureBox3.ImageLocation = card.Image;
            pictureBox3.SizeMode = PictureBoxSizeMode.AutoSize;

            sumBankerCards();

            while (bankercardSum < dealerdifficulty)
            {
                int randCard = selectRandomCard();
                while (usedCards.Contains(randCard))
                {
                    randCard = selectRandomCard();
                }
                randCard = 1 * randCard;
                Card crd = deck[randCard];
                usedCards.Add(randCard);

                bankercardList.Add(crd);

                PictureBox p4 = new PictureBox();
                p4.Width = 71;
                p4.Height = 96;
                p4.Location = new Point(531 + bankerbox.Count * 76, 48);
                pictureBox3.SendToBack();
                p4.ImageLocation = crd.Image;
                p4.SizeMode = PictureBoxSizeMode.AutoSize;
                this.Controls.Add(p4);

                sumBankerCards();
                bankerbox.Add(p4);
                bankercardList.Add(crd);
            }
        }
        private void player3move()
        {
            int randomCard = selectRandomCard();
            while (usedCards.Contains(randomCard))
            {
                randomCard = selectRandomCard();
            }
            randomCard = 1 * randomCard;
            Card card = deck[randomCard];
            usedCards.Add(randomCard);

            player3cardList.Add(card);

            cardBoxP32.ImageLocation = card.Image;
            cardBoxP32.SizeMode = PictureBoxSizeMode.AutoSize;
            sumPlayer3Cards();

            while (player3cardSum < playerdifficulty)
            {
                int randCard = selectRandomCard();
                while (usedCards.Contains(randCard))
                {
                    randCard = selectRandomCard();
                }
                randCard = 1 * randCard;
                Card crd = deck[randCard];
                usedCards.Add(randCard);

                player3cardList.Add(crd);

                PictureBox p7 = new PictureBox();
                p7.Width = 71;
                p7.Height = 96;
                p7.Location = new Point(878 + player3box.Count * 76, 302);
                cardBoxP22.SendToBack();
                p7.ImageLocation = crd.Image;
                p7.SizeMode = PictureBoxSizeMode.AutoSize;
                this.Controls.Add(p7);

                sumPlayer3Cards();
                player3box.Add(p7);
                player3cardList.Add(crd);

            }
        }
        private void player2move()
        {
            int randomCard = selectRandomCard();
            while (usedCards.Contains(randomCard))
            {
                randomCard = selectRandomCard();
            }
            randomCard = 1 * randomCard;
            Card card = deck[randomCard];
            usedCards.Add(randomCard);

            player2cardList.Add(card);

            cardBoxP22.ImageLocation = card.Image;
            cardBoxP22.SizeMode = PictureBoxSizeMode.AutoSize;
            sumPlayer2Cards();

            while (player2cardSum < playerdifficulty)
            {
                int randCard = selectRandomCard();
                while (usedCards.Contains(randCard))
                {
                    randCard = selectRandomCard();
                }
                randCard = 1 * randCard;
                Card crd = deck[randCard];
                usedCards.Add(randCard);

                player2cardList.Add(crd);

                PictureBox p8 = new PictureBox();
                p8.Width = 71;
                p8.Height = 96;
                p8.Location = new Point(878 + player2box.Count * 76, 174);
                cardBoxP22.SendToBack();
                p8.ImageLocation = crd.Image;
                p8.SizeMode = PictureBoxSizeMode.AutoSize;
                this.Controls.Add(p8);

                sumPlayer2Cards();
                player2box.Add(p8);
                player2cardList.Add(crd);

            }


        }
        private void player1move()
        {
            int randomCard = selectRandomCard();
            while (usedCards.Contains(randomCard))
            {
                randomCard = selectRandomCard();
            }
            randomCard = 1 * randomCard;
            Card card = deck[randomCard];
            usedCards.Add(randomCard);

            player1cardList.Add(card);

            cardBoxP12.ImageLocation = card.Image;
            cardBoxP12.SizeMode = PictureBoxSizeMode.AutoSize;
            sumPlayer1Cards();

            while (player1cardSum < playerdifficulty)
            {
                int randCard = selectRandomCard();
                while (usedCards.Contains(randCard))
                {
                    randCard = selectRandomCard();
                }
                randCard = 1 * randCard;
                Card crd = deck[randCard];
                usedCards.Add(randCard);

                player1cardList.Add(crd);

                PictureBox p9 = new PictureBox();
                p9.Width = 71;
                p9.Height = 96;
                p9.Location = new Point(878 + player1box.Count * 76, 48);
                cardBoxP12.SendToBack();
                p9.ImageLocation = crd.Image;
                p9.SizeMode = PictureBoxSizeMode.AutoSize;
                this.Controls.Add(p9);

                sumPlayer1Cards();
                player1box.Add(p9);
                player1cardList.Add(crd);

            }


        }
    }
}



